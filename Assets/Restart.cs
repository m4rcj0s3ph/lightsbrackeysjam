﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Restart : MonoBehaviour {
    public Image fadeImage;
    public TextMeshProUGUI tyMessage;

    private bool _isOk = false;
    // Use this for initialization
    void Start () {
        StartCoroutine(fade());
	}
	
    IEnumerator fade()
    {
        yield return new WaitForSeconds(7f);
        _isOk = true;
    }
	// Update is called once per frame
	void Update () {
        if (fadeImage.color.a < 1 && _isOk)
        {
            fadeImage.color += Color.black * Time.deltaTime * 1f;
            tyMessage.color += Color.black * Time.deltaTime * 1f;
        }
    }
}
