﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class moveGoal : MonoBehaviour {
    public float velocity = 5f;
    public menuListener mListener;

    private Transform _this;
    // Use this for initialization
    void Start()
    {
        _this = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        _this.position += Vector3.down * Time.deltaTime * velocity;
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        mListener.OnStartBtn();
        //Application.LoadLevel(Application.loadedLevel);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
