﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveMenu : MonoBehaviour {
    public float velocity = 0.1f;
    public float limit = 0.5f;
    private Transform _this;
    // Use this for initialization
    void Start()
    {
        _this = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        _this.position += Vector3.right * Time.deltaTime * velocity;
        if(_this.position.x > limit || _this.position.x < - limit)
        {
            velocity = -velocity;
        }
    }
}
