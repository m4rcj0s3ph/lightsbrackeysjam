﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class menuListener : MonoBehaviour {
    public Image fadeImage;
    public float timeFade = 1f;
    public bool isLevel = false;
    public TextMeshProUGUI titleLevel;

    private bool _fadeState = false;
    void Start()
    {
        if(isLevel) StartCoroutine(TitleAnimation());
    }
    IEnumerator TitleAnimation()
    {
        yield return new WaitForSeconds(timeFade);
        titleLevel.color += Color.black;
        yield return new WaitForSeconds(timeFade);
        titleLevel.color -= Color.black;
    }
    void Update()
    {
        if (_fadeState == true && fadeImage.color.a < 1)
        {
            fadeImage.color += Color.black * timeFade * Time.deltaTime;
        }
        if (_fadeState == false && fadeImage.color.a > 0)
        {
            fadeImage.color -= Color.black * timeFade * Time.deltaTime;
        }
    }
	public void OnStartBtn ()
    {
        _fadeState = true;
        StartCoroutine(LoadScene());
    }
    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(timeFade);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void OnExitBtn()
    {
        Debug.Log("Exit");
        Application.Quit();
    }
}
