﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class enemyConfig
{
    public bool left;
    public bool middle;
    public bool right;
    public int position
    {
        get { return (right ? 4 : 0) + (middle ? 2 : 0) + (left ? 1 : 0); }
    }
    public float timeCreate;
    public float velocity = 0;
}
public class enemyController : MonoBehaviour {
    public GameObject enemyPrefab;
    public GameObject goalObject;
    public List<GameObject> enemysInstance;
    public List<enemyConfig> enemysLeftConfig;
    public List<enemyConfig> enemysRightConfig;

    private bool _leftEnd = false;
    private bool _rightEnd = false;
    private int _countELeftC = 0; //Count enemy Left config
    private int _countERightC = 0; //Count enemy Right config
    private int _countILeft = 0; //Count instance Left
    private int _countIRight = 5; //Count instance Right

    // Use this for initialization
    void Start ()
    {
        for(int i = 0; i < 10; i++)
        {
            enemysInstance.Add(Instantiate(enemyPrefab));
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(!_leftEnd)
        {
            if (enemysLeftConfig[_countELeftC].timeCreate <= Time.timeSinceLevelLoad)
            {
                if (enemysLeftConfig[_countELeftC].position != 0)
                {
                    if (enemysLeftConfig[_countELeftC].left)
                    {
                        SetPositionLeft(-6);
                    }
                    if (enemysLeftConfig[_countELeftC].middle)
                    {
                        SetPositionLeft(-4);
                    }
                    if (enemysLeftConfig[_countELeftC].right)
                    {
                        SetPositionLeft(-2);
                    }
                }
                _countELeftC++;
                if (enemysLeftConfig.Count <= _countELeftC)
                {
                    _leftEnd = true;
                }
            }
        }
        if (!_rightEnd)
        {
            if (enemysRightConfig[_countERightC].timeCreate <= Time.timeSinceLevelLoad)
            {
                if (enemysRightConfig[_countERightC].position != 0)
                {
                    if (enemysRightConfig[_countERightC].left)
                    {
                        SetPositionRight(2);
                    }
                    if (enemysRightConfig[_countERightC].middle)
                    {
                        SetPositionRight(4);
                    }
                    if (enemysRightConfig[_countERightC].right)
                    {
                        SetPositionRight(6);
                    }
                }
                _countERightC++;
                if (enemysLeftConfig.Count <= _countERightC)
                {
                    _rightEnd = true;
                }
            }
        }

        if (_leftEnd && _rightEnd) goalObject.SetActive(true);
    }
    public void SetPositionLeft(int position)
    {
        _countILeft = SetPosition(position, _countILeft, 
            enemysLeftConfig[_countELeftC].velocity, 4, 0);
    }
    public void SetPositionRight(int position)
    {
        _countIRight = SetPosition(position, _countIRight,
            enemysRightConfig[_countERightC].velocity, 9, 5);
    }
    public int SetPosition(int position, int countInst, float velocity, int limit, int valueReturn)
    {        
        enemysInstance[countInst].transform.position = new Vector3(position, 7, 0);
        if (velocity != 0)
        {
            moveEnemy tempEnemyI = enemysInstance[countInst].GetComponent<moveEnemy>();
            tempEnemyI.velocity = velocity;
        }
        if(!enemysInstance[countInst].activeSelf) enemysInstance[countInst].SetActive(true);
        countInst++;
        if (countInst > limit) countInst = valueReturn;
        return countInst;
    }
}
