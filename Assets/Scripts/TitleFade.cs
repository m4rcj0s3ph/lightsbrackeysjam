﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleFade : MonoBehaviour {

    public Renderer rendTitle;
    // Use this for initialization
    void Start () {
        rendTitle = GetComponent<Renderer>();
        rendTitle.material.shader = Shader.Find("Dilate");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
