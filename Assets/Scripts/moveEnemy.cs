﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class moveEnemy : MonoBehaviour {
    public float velocity = 5f;

    private bool _isShock = false;
    private AudioSource _shock;
    private Transform _this;
	// Use this for initialization
	void Start ()
    {
        _this = GetComponent<Transform>();
        _shock = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        _this.position += Vector3.down * Time.deltaTime * velocity;		
	}
    void OnTriggerEnter2D(Collider2D col)
    {
        if(!_isShock)
        {
            _shock.Play();
            _isShock = true;
            StartCoroutine(ReplayLevel());
        }        
    }
    IEnumerator ReplayLevel()
    {
        Time.timeScale = 0f;
        float pauseEndTime = Time.realtimeSinceStartup + 0.3f;
        while (Time.realtimeSinceStartup < pauseEndTime) yield return 0;
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
