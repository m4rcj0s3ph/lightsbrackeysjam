﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class characterController : MonoBehaviour {
    public Transform leftCharacter;
    public Transform rightCharacter;
    public Image leftFade;
    public Image rightFade;
    public Slider sliderBattery;

    private bool _fadeLState = false;
    private bool _fadeRState = false;
    private float faulTime = 0f;
    private bool faul = false;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.A) && leftCharacter.position.x > -6)
        {
            leftCharacter.position -= transform.right * 2;
        }
        if (Input.GetKeyDown(KeyCode.D) && leftCharacter.position.x < -2)
        {
            leftCharacter.position += transform.right * 2;
        }
        if (Input.GetKeyDown(KeyCode.J) && rightCharacter.position.x > 2)
        {
            rightCharacter.position -= transform.right * 2;
        }
        if (Input.GetKeyDown(KeyCode.L) && rightCharacter.position.x < 6)
        {
            rightCharacter.position += transform.right * 2;
        }

        if(!faul)
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                _fadeLState = !_fadeLState;
            }

            if (Input.GetKeyDown(KeyCode.K))
            {
                _fadeRState = !_fadeRState;
            }
        }
        if (_fadeLState == true && leftFade.color.a < 1)
        {
            leftFade.color += Color.black * 0.1f;
        }
        if (_fadeLState == false && leftFade.color.a > 0)
        {
            leftFade.color -= Color.black * 0.1f;
        }

        if (_fadeRState == true && rightFade.color.a < 1)
        {
            rightFade.color += Color.black * 0.1f;
        }
        if (_fadeRState == false && rightFade.color.a > 0)
        {
            rightFade.color -= Color.black * 0.1f;
        }

        if (_fadeLState == true && _fadeRState == true)
        {
            sliderBattery.value += 0.075f * Time.deltaTime;
        }
        else if (_fadeLState == false && _fadeRState == false)
        {
            sliderBattery.value -= 0.05f * Time.deltaTime;
        }

        if (sliderBattery.value == 0f)
        {
            faul = true;
            faulTime = Time.time + 3f;
            _fadeLState = true;
            _fadeRState = true;
        }
        if (faulTime <= Time.time && faul)
        {
            faul = false;
        }
    }
}
